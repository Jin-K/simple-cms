﻿using IdentityServer4.Models;

namespace SimpleCRM.Auth.Models {
  public class ErrorViewModel {
    public ErrorMessage Error { get; set; }
  }
}
