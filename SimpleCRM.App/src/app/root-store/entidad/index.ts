import * as EntidadStoreActions    from './actions';
import * as EntidadStoreSelectors  from './selectors';
import * as EntidadStoreState      from './state';

export {
  EntidadStoreActions,
  EntidadStoreSelectors,
  EntidadStoreState
};
