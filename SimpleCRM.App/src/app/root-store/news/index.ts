import * as NewsStoreActions    from './actions';
import * as NewsStoreState      from './state';

export {
  NewsModule
} from './news.module';

export {
  NewsStoreActions,
  NewsStoreState
};
