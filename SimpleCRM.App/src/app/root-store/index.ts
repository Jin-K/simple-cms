import { RootStoreModule }  from './root-store.module';
import * as NewsActions     from './news/actions';

export { RootStoreModule, NewsActions };
