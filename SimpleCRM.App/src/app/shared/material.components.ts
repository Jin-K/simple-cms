import * as fromMaterial from '@angular/material';

export const MaterialComponents = [
  fromMaterial.MatButtonModule,
  fromMaterial.MatMenuModule,
  fromMaterial.MatCheckboxModule,
  fromMaterial.MatToolbarModule,
  fromMaterial.MatFormFieldModule,
  fromMaterial.MatInputModule,
  fromMaterial.MatProgressSpinnerModule,
  fromMaterial.MatProgressBarModule,
  fromMaterial.MatCardModule,
  fromMaterial.MatIconModule,
  fromMaterial.MatSidenavModule,
  fromMaterial.MatTabsModule
];
